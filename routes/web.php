<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\MessageController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\WomanController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Test git push for login

Route::get('/', function () {
    return view('login');
});

Route::get('login', [AuthController::class, 'loginWithGoogle'])->name('login');
Route::prefix('google')->name('google.')->group(function () {
    Route::get('login', [AuthController::class, 'loginWithGoogle'])->name('login');
    Route::any('callback', [AuthController::class, 'callbackFromGoogle'])->name('callback');
});
Route::middleware('auth')->group(function () {
    Route::get('top-list', [UserController::class, 'getTopList'])->name('top-list');
    Route::get('lady/{id}', [WomanController::class, 'getProfile'])->name('profile');
    Route::post('send-message', [MessageController::class, 'sendMessage'])->name('send-message');
});
