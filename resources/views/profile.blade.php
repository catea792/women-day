<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('assets/css/profile.css') }}">
    <style>
        @media (max-width: 740px) {
            .page-container {
                background-image: linear-gradient(0, rgba(233, 147, 168, 0.5), rgba(251, 126, 182, 0.5)), url({{ $profile->image }});
                background-repeat: no-repeat;
                background-size: cover;
                background-position: center;
                height: 100vh;
                margin: 0 auto;
            }
        }
    </style>
</head>

<body>
    <div class="page-container">
        <div class="display-message top-0 start-50 translate-middle-x position-fixed mt-2"></div>
        <div class="d-flex justify-content-between">
            <button class="btn">
                <svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="currentColor"
                    class="bi bi-arrow-left">
                    <path fill-rule="evenodd"
                        d="M15 8a.5.5 0 0 0-.5-.5H2.707l3.147-3.146a.5.5 0 1 0-.708-.708l-4 4a.5.5 0 0 0 0 .708l4 4a.5.5 0 0 0 .708-.708L2.707 8.5H14.5A.5.5 0 0 0 15 8z" />
                </svg>
            </button>
        </div>
        <div class="content">
            <div class="top-header d-flex justify-content-center mt-2">
                <h1>{{ $profile->name }}</h1>
            </div>
            <div class="image">
                <img src="{{ $profile->image }}" class="rounded-circle mx-auto d-block" alt="...">
            </div>
            <div class="messages-list py-3">
                <div class="overflow-auto scrollbar">
                    @foreach ($profile->messages as $message)
                        <div class="force-overflow">
                            <div class="card m-2 shadow-sm">
                                <div class="card-body">
                                    <b class="card-subtitle mb-2 text-muted border-bottom">{{ $message->user->email }}
                                    </b>
                                    <p class="card-text">
                                        {{ $message->content }}
                                    </p>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="px-4 py-2 message-input">
            <div class="input-group d-flex mx-1 justify-content-between">
                <input type="text" class="form-control message rounded" id="message"
                    placeholder="Gửi lời chúc đến {{ $profile->name }}">
                <button class="btn rounded-circle btn-send-messsage" style="color:rgb(255, 255, 255)">
                    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor"
                        class="bi bi-send-fill" viewBox="0 0 20 20">
                        <path
                            d="M15.964.686a.5.5 0 0 0-.65-.65L.767 5.855H.766l-.452.18a.5.5 0 0 0-.082.887l.41.26.001.002 4.995 3.178 3.178 4.995.002.002.26.41a.5.5 0 0 0 .886-.083l6-15Zm-1.833 1.89L6.637 10.07l-.215-.338a.5.5 0 0 0-.154-.154l-.338-.215 7.494-7.494 1.178-.471-.47 1.178Z" />
                    </svg>
                </button>
            </div>
        </div>
    </div>
</body>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js"
    integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN" crossorigin="anonymous">
</script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.3/jquery.min.js"></script>
<script>
    function sendMessage() {
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: "POST",
            url: "{{ route('send-message') }}",
            data: {
                woman_id: {{ $profile->id }},
                content: $('.message').val(),
            },
            success: function(data) {
                $(".display-message").html(data, 1000);
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                alert(errorThrown);
            }
        });
    }

    function checkEmptyMessage() {

    }
    $(document).ready(function() {
        $('body').delegate('.btn-send-messsage', 'click', function(e) {
            if (confirm("Bạn chỉ có thể gửi 1 lời nhắn cho 1 người!!") == true) {
                sendMessage()
            }
        })
    });
</script>

</html>
