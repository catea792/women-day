<?php

namespace Database\Seeders;

use App\Models\Woman;
use Illuminate\Database\Seeder;

class WomanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $women = [
            [
                'email'  => 'user1@example.com',
                'name'   => 'Nguyễn Thị Uyên',
                'avatar' => 'https://drive.google.com/file/d/1MbIn9J_sy4RAGslC_lltHB7SEkkblccT/view?usp=sharing',
                'team'   => 'Developer'
            ],
            [
                'email'  => 'user2@example.com',
                'name'   => 'Đỗ Thị Hương',
                'avatar' => 'https://drive.google.com/file/d/13I7aNJEjBYVKFcQeCLkkiT4taIR3JWYG/view?usp=sharing',
                'team'   => 'Developer'
            ],
            [
                'email'  => 'user3@example.com',
                'name'   => 'Mai Thị Gấm',
                'avatar' => 'https://drive.google.com/file/d/1Riop5cLZArnD5mAelZrAGVd9RBE8g8it/view?usp=sharing',
                'team'   => 'Developer'
            ],
            [
                'email'  => 'user4@example.com',
                'name'   => 'Bùi Thị Quỳnh',
                'avatar' => 'https://drive.google.com/file/d/18hVWI9WZerlKvPehV0qDjyAHrUtNPro_/view?usp=sharing',
                'team'   => 'Tester'
            ],
            [
                'email'  => 'user5@example.com',
                'name'   => 'Nguyễn Thị Hạnh',
                'avatar' => 'https://drive.google.com/file/d/1Yf8iUIT4gR0sNyO65TSsPqjlKULZX5Jr/view?usp=sharing',
                'team'   => 'Tester'
            ],
            [
                'email'  => 'user6@example.com',
                'name'   => 'Hồ Thị Bình An',
                'avatar' => 'https://drive.google.com/file/d/1lBuhcGFmaA3794p8qSOD8PqNXyBdYERx/view?usp=sharing',
                'team'   => 'Comtor'
            ],
            [
                'email'  => 'user7@example.com',
                'name'   => 'Hoàng Lan Hương',
                'avatar' => 'https://drive.google.com/file/d/1dkJRPp1Jpc5YgcdJ5eTmB9LK7ZoNpV6i/view?usp=sharing',
                'team'   => 'Comtor'
            ],
            [
                'email'  => 'user8@example.com',
                'name'   => 'Lê Thị Vân',
                'avatar' => 'https://drive.google.com/file/d/1_OT-FZuPT6Clv33btdVARJSIV6hBv2Mo/view?usp=sharing',
                'team'   => 'Comtor'
            ],
            [
                'email'  => 'user9@example.com',
                'name'   => 'Trần Thị Mỹ Duyên',
                'avatar' => 'https://drive.google.com/file/d/1IZLFHZa8I2eZ8_3njk428WdUAprqie2X/view?usp=sharing',
                'team'   => 'Comtor'
            ],
            [
                'email'  => 'user10@example.com',
                'name'   => 'Đàm Thị Hải Yến',
                'avatar' => 'https://drive.google.com/file/d/1vrUNuc8e9teVF4R53PgNKrbIz7TJYsru/view?usp=sharing',
                'team'   => 'Comtor'
            ],
            [
                'email'  => 'user11@example.com',
                'name'   => 'Khuất Thu Trang',
                'avatar' => 'https://drive.google.com/file/d/17hVtijVZ1kcoOn4d-7Cvbb5xj8Op4L--/view?usp=sharing',
                'team'   => 'Comtor'
            ],
            [
                'email'  => 'user12@example.com',
                'name'   => 'Lường Thị Bích',
                'avatar' => 'https://drive.google.com/file/d/1WlJYHlt8iq8NaaU92vKwvQ48C574jW-Z/view?usp=sharing',
                'team'   => 'HR-BO'
            ],
            [
                'email'  => 'user13@example.com',
                'name'   => 'Trương Yến Nhi',
                'avatar' => 'https://drive.google.com/file/d/1ES8JRj2RcQhuXdtSfpPMc2ulXpW-5t6l/view?usp=share_link',
                'team'   => 'HR-BO'
            ],
            [
                'email'  => 'user14@example.com',
                'name'   => 'Trần Thị Hồng',
                'avatar' => 'https://drive.google.com/file/d/1ltUBtbt0-3QuDzfHryHsDvjkNejRCUhE/view?usp=sharing',
                'team'   => 'HR-BO'
            ],
            [
                'email'  => 'user15@example.com',
                'name'   => 'Đinh Thị Cẩm Ly',
                'avatar' => 'https://drive.google.com/file/d/1lBKhS84JZEKyZ91wijE225bU_ebN-8Ea/view?usp=sharing',
                'team'   => 'Tester'
            ],
            [
                'email'  => 'user16@example.com',
                'name'   => 'Hoàng Xuân Quỳnh',
                'avatar' => 'https://drive.google.com/file/d/1yc5ppq-CxKH8uiHpRzmoaFYftLqXJsef/view?usp=sharing',
                'team'   => 'Comtor'
            ],
            [
                'email'  => 'user17@example.com',
                'name'   => 'Hà Ngọc Ánh',
                'avatar' => 'https://drive.google.com/file/d/1OgjGP1dZ7ccd-91c1t0OmQ2wrstw2kul/view?usp=sharing',
                'team'   => 'Developer'
            ],
            [
                'email'  => 'user18@example.com',
                'name'   => 'Nguyễn Ngọc Hiền',
                'avatar' => 'https://drive.google.com/file/d/1DhQQb9wwcYBM3qfiwfo7kgqr5KfaQZeA/view?usp=sharing',
                'team'   => 'Tester'
            ],
            [
                'email'  => 'user19@example.com',
                'name'   => 'Nguyễn Thị Hà Giang',
                'avatar' => 'https://drive.google.com/file/d/14wmCU3Ialp1-QPHFJ1G0Qyyef9FyPxzF/view?usp=sharing',
                'team'   => 'Tester'
            ],
            [
                'email'  => 'user20@example.com',
                'name'   => 'Nguyễn Thị Thúy',
                'avatar' => 'https://drive.google.com/file/d/1p3r1hZznJQ_ml-mvVYhziimFKdgpGh7L/view?usp=sharing',
                'team'   => 'Tester'
            ],
            [
                'email'  => 'user21@example.com',
                'name'   => 'Trần Thị Thanh Mai',
                'avatar' => 'https://drive.google.com/file/d/1nWjqzkJ52VfFnnAfJQLc0d15bSYGBdSZ/view?usp=sharing',
                'team'   => 'Tester'
            ],
            [
                'email'  => 'user22@example.com',
                'name'   => 'Lê Thị Tươi',
                'avatar' => 'https://drive.google.com/file/d/1Io05aje1WJQY2Zt-dkJi7bJJCPZVM3sg/view?usp=sharing',
                'team'   => 'Subbrse'
            ],
            [
                'email'  => 'user23@example.com',
                'name'   => 'Nguyễn Thị Hà',
                'avatar' => 'https://drive.google.com/file/d/1FU59KlSyxyuvxESiWd2sib8SxSoDsSNp/view?usp=sharing',
                'team'   => 'Tester'
            ],
            [
                'email'  => 'user24@example.com',
                'name'   => 'Lê Hà Phương',
                'avatar' => 'https://drive.google.com/file/d/1Eri35bbbkzpMbB4GwuuUjLOpjbmhp12K/view?usp=sharing',
                'team'   => 'Thực tập sinh'
            ],
            [
                'email'  => 'user25@example.com',
                'name'   => 'Nguyễn Thị Điệp',
                'avatar' => 'https://drive.google.com/file/d/178q5uEwiVxMKlWWHjbRN7mYs6CFT50Z6/view?usp=sharing',
                'team'   => 'Comtor'
            ],
            [
                'email'  => 'user26@example.com',
                'name'   => 'Chu Hà My',
                'avatar' => 'https://drive.google.com/file/d/1suyfExKMqAvzi4_OYUkdnfTRAo7IG2s5/view?usp=sharing',
                'team'   => 'Tester'
            ],
            [
                'email'  => 'user27@example.com',
                'name'   => 'Nguyễn Thị Hương',
                'avatar' => 'https://drive.google.com/file/d/1dRBmBQvd3Yo3Uw0umrfsGs2AkCNFhFpx/view?usp=sharing',
                'team'   => 'Developer'
            ],
            [
                'email'  => 'user28@example.com',
                'name'   => 'Nguyễn Thị Thanh Thảo',
                'avatar' => 'https://drive.google.com/file/d/1zdk67RNlsWSthOQPONKLDxz8IU4jcslh/view?usp=sharing',
                'team'   => 'Comtor'
            ],
            [
                'email'  => 'user29@example.com',
                'name'   => 'Lê Thu Hồng',
                'avatar' => 'https://drive.google.com/file/d/1Ic18LHo1zxjdhmIJdYYKD9gjKrg8zgzW/view?usp=sharing',
                'team'   => 'HR'
            ],
            [
                'email'  => 'user30@example.com',
                'name'   => 'Hoàng Thị Hương Bình',
                'avatar' => 'https://drive.google.com/file/d/1h9jPC0aWfIRnCYPK8Xzr0PwffSyqYJJN/view?usp=sharing',
                'team'   => 'Subbrse'
            ],
            [
                'email'  => 'user31@example.com',
                'name'   => 'Đào Thị Thu Trang',
                'avatar' => 'https://drive.google.com/file/d/1Dgerflp0oPGCVLXbp0XphsQTUmSDEVeA/view?usp=sharing',
                'team'   => 'Thực tập sinh'
            ],
            [
                'email'  => 'user32@example.com',
                'name'   => 'Nguyễn Thị Khánh Ly',
                'avatar' => 'https://drive.google.com/file/d/17FqbJerLLGmvzEAywDldMmgNAuXuI7Hj/view?usp=sharing',
                'team'   => 'Tester'
            ],
            [
                'email'  => 'user33@example.com',
                'name'   => 'Nguyễn Thị Hồng Thúy',
                'avatar' => 'https://drive.google.com/file/d/126r25T0GmPWTeULzHYRgyk9RmkrLnpTs/view?usp=sharing',
                'team'   => 'Tester'
            ],
            [
                'email'  => 'user34@example.com',
                'name'   => 'Nguyễn Hà Linh',
                'avatar' => 'https://drive.google.com/file/d/1DRyck71D34ScbSsQO0RCSuUaOfQaNnKR/view?usp=sharing',
                'team'   => 'Tester'
            ],
            [
                'email'  => 'user35@example.com',
                'name'   => 'Hồ Thị Oanh',
                'avatar' => 'https://drive.google.com/file/d/1TbewMnDNWMijXGQgchqxqwczXmg80iZ2/view?usp=sharing',
                'team'   => 'Thực tập sinh'
            ],
            [
                'email'  => 'user36@example.com',
                'name'   => 'Vũ Thảo Vi',
                'avatar' => 'https://drive.google.com/file/d/1WEknmj7mLHE4kpsu2ANVtubWyUxX_UCP/view?usp=sharing',
                'team'   => 'Thực tập sinh'
            ],
            [
                'email'  => 'user37@example.com',
                'name'   => 'Hà Thị Tú Anh',
                'avatar' => 'https://drive.google.com/file/d/1QRvTSiFA0Mfftjf8qB9ykF9B3S7mJxQp/view?usp=sharing',
                'team'   => 'Tester'
            ],
        ];

        foreach ($women as $womanInfo) {
            Woman::query()->create($womanInfo);
        }
    }
}
