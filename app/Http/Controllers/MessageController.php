<?php

namespace App\Http\Controllers;

use App\Http\Requests\MessageRequest;
use App\Models\Message;

class MessageController extends Controller
{
    public function sendMessage(MessageRequest $request)
    {
        $data = $request->validated();
        $data['user_id'] = auth()->id();
        $message = Message::where([
            ['user_id', $data['user_id']],
            ['woman_id', $data['woman_id']]
        ])->exists();
        if ($message) {
            return view('toast', ['error' => 'Bạn đã gửi lời chúc đến người này ròi!!']);
        }
        $message = Message::create($data);
        return view('toast', ['success' => 'Gửi lời chúc thành công!!']);
    }
}
