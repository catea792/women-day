<?php

namespace App\Http\Controllers;

use App\Models\Woman;
use Illuminate\Http\Request;

class WomanController extends Controller
{
    public function getProfile($id)
    {
        $profile = Woman::where('id', $id)->with('messages')->firstOrFail();
        $profile = Woman::where('id', $id)->with("messages", function ($messages) {
            $messages->with("user");
        })->firstOrFail();
        return view('profile', compact('profile'));
    }
}
